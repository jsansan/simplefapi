from typing import Optional, List
from fastapi import FastAPI, HTTPException
from fastapi.encoders import jsonable_encoder #Importación incluida para el update
from pydantic import BaseModel

app = FastAPI()

class Item (BaseModel):
    name: str
    price: float
    is_offer: Optional[bool] = None

#Complemente la variable definiendo su tipo usando typing
# RESPUESTA:
fakeDB: List[Item]

@app.get("/")
def read_root():
    #Documente que hace el código de esta función y proponga una mejor solución de código
    # a = 0
    # for i in fakeDB:
    #     a += 1 if fakeDB[i].price > 0 else 0
    # return a

    # RESPUESTA = Este codigo básicamente incrementa la variable 'a' por cada 
    # elemento en fakeDB cuyo precio es mayor a cero.
    # Una forma un poco más eficiente sería esta:
    a  = sum(1 for i in fakeDB if fakeDB[i].price > 0)
    return a


@app.get("/items/{item_id}")
async def read_item(item_id: int):
    # Implemente un sistema de error en caso de que no exista el item solicitado

    # RESPUESTA: elevación de error manualmente
    if item_id not in fakeDB:
        raise HTTPException(status_code=404, detail="Item no encontrado")
    return fakeDB[item_id]


@app.put("/items/{item_id}", response_model=Item)
async def update_item(item_id: int, item: Item):
    #Implemente el código correspondiente para actualizar un item

    #RESPUESTA:
    update_item_encoded = jsonable_encoder(item)
    fakeDB[item_id] = update_item_encoded
    return update_item_encoded


@app.post("/items/{item_id}", response_model=Item)
async def create_item(item_id: int, item: Item):
    #Implemente un sistema de error en caso de que el item ya exista

    #RESPUESTA = Levanto una manual, sin embargo, la documentación de FastApi especifica que al item ser una instancia definida de sus modelos, ya posee las validaciones necesarias y son mostradas en el consumo del servicio.
    if item_id in fakeDB:
        raise HTTPException(status_code=404, detail="Item ya existe")
    else:
        fakeDB.append(item)
    return item
